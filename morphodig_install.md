
# MorphoDig Build & Install

## vtk

```
sudo apt install build-essential qtbase5-dev cmake-curses-gui libxt-dev

wget https://github.com/Kitware/VTK/archive/refs/tags/v9.0.0.tar.gz
tar xzfv v9.0.0.tar.gz

mkdir vtk9.0-build
cd vtk9.0-build

# cmake ../VTK-9.0.0 -DVTK_Group_Imaging=ON -DVTK_Group_Qt=ON -DVTK_Group_Rendering=ON -DVTK_Group_StandAlone=ON -DVTK_Group_Views=ON
# https://discourse.vtk.org/t/9-0-0-rc1-contextopengl2-not-built/2933

cmake ../VTK-9.0.0 -DVTK_Group_Imaging=ON -DVTK_Group_Qt=ON -DVTK_Group_Rendering=ON -DVTK_Group_StandAlone=ON -DVTK_Group_Views=ON -DVTK_MODULE_ENABLE_VTK_RenderingContextOpenGL2=YES

make
sudo make install
```

## cmake

the latest cmake build isn't in the ubuntu repo, you need to install it as well

```
sudo snap install cmake --classic

```

## morphodig

```
git clone https://github.com/morphomuseum/MorphoDig
mkdir morphodig-build
cd morphodig-build
cmake ../MorphoDig
make
```

## Run the fricking software

```
./MorphoDig/MorphoDig
```


